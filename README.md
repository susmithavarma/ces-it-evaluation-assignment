I have used HTML5, CSS3, JavaScript, Font awesome and Bootstarp4 in this assignment(Assignment 1, Assignment 2).

I have incorporated links in HTML 
CSS3 : 

<link rel="stylesheet" type="text/css" href="css/index.css">

Bootstrap4 and Jquery: 

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

JavaScript:

<script src="scripts/functions.js"></script>

Font awesome:

<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
    integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

Js Code style from the link (https://www.w3schools.com/js/js_best_practices.asp)

Sample code: 

// function_1
/* Takes an array as a parameter and returns a new array that 
consists of values merged twice from the initial array*/

function merge(array1) { //array1 is input value
    var array = new Array(array1), //array1 input value is stored in new array
        array2 = array.concat(array); //array2 is the output
    document.getElementById("output_1").innerHTML = '[' + array2 + ']';
}

I have followed the style guide and coding conventions.



