// declaring variable globally
var data = [];
var index = 0;

function editData(i) {
    document.getElementById("save").style.display = "none";
    document.getElementById("update").style.display = "inline-block";
    var iValue = data[i];
    index = i;
    document.getElementById("item").value = iValue;
}

function deleteData(i) {
    var iValue = data[i];
    if (confirm("Are you sure! you want to delete " + iValue)) { // confirmation message
        if (i > -1) {
            data.splice(i, 1); // splice() removes the items from array
        }
        // refresh table after item is deleted
        var objLength = data.length;
        var table = '<table class="table">' +
            '<thead><tr>' +
            '<th class="col-sm-4">To do Lists</th>' +
            '</tr></thead><tbody>';

        for (var i = 0; i < objLength; i++) {
            table += '<tr>' +
                '<td>' + data[i] + '<button class="btn delete" onClick="deleteData(' + i + ')"><i class="fa fa-trash-o"  aria-hidden="true"></i></button>' +
                '<button class="btn edit" onClick="editData(' + i + ')"><i class="fa fa-pencil"  aria-hidden="true"></i></button></td>' +
                '</tr>'
        }

        table += '</tbody></table>';

        document.getElementById('tab').innerHTML = table;
        setTimeout(function () {
            alert("deleted successfully");
        }, 200);

    }
}

function update() {
    data[index] = document.getElementById("item").value;
    document.getElementById("item").value = "";
    document.getElementById("update").style.display = "none";
    document.getElementById("save").style.display = "inline-block";
    //creating table for storing values dynamically
    var objLength = data.length;
    var table = '<table class="table">' +
        '<thead><tr>' +
        '<th class="col-sm-4">To do Lists</th>' +
        '</tr></thead><tbody>';

    for (var i = 0; i < objLength; i++) {
        table += '<tr>' +
            '<td>' + data[i] + '<button class="btn delete" onClick="deleteData(' + i + ')"><i class="fa fa-trash-o"  aria-hidden="true"></i></button>' +
            '<button class="btn edit" onClick="editData(' + i + ')"><i class="fa fa-pencil"  aria-hidden="true"></i></button></td>' +
            '</tr>'
    }

    table += '</tbody></table>';

    document.getElementById('tab').innerHTML = table;
    setTimeout(function () {
        alert("updated successfully");
    }, 200);

}

function save() {

    data.push(document.getElementById("item").value); // Insert Input value into data array
    document.getElementById("item").value = ""; // Input value should be empty
    //creating table for storing values from array 
    var objLength = data.length;
    var table = '<table class="table">' +
        '<thead><tr>' +
        '<th class="col-sm-4">To do Lists</th>' +
        '</tr></thead><tbody>';

    for (var i = 0; i < objLength; i++) {
        table += '<tr>' +
            '<td>' + data[i] + '<button class="btn delete" onClick="deleteData(' + i + ')"><i class="fa fa-trash-o"  aria-hidden="true"></i></button>' +
            '<button class="btn edit" onClick="editData(' + i + ')"><i class="fa fa-pencil"  aria-hidden="true"></i></button></td>' +
            '</tr>'
    }

    table += '</tbody></table>';

    document.getElementById('tab').innerHTML = table;
}